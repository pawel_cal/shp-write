module.exports.point = justType('Point', 'POINT');
module.exports.line = justType('LineString', 'POLYLINE');
module.exports.polygon = justType('Polygon', 'POLYGON');

function justType(type, TYPE) {
	return function (gj) {
		const oftype = gj.features.filter(isType(type));
		return {
			geometries: oftype.map(justCoords),
			properties: oftype.map(justProps),
			type: TYPE
		};
	};
}

function justCoords(f) {
	if (!f.geometry) {
		return [];
	}
	const type = f.geometry.type;
	const multi = type.startsWith('Multi');
	const point = type.endsWith('Point');
	return point || multi ? f.geometry.coordinates : [f.geometry.coordinates];
	//wyjściowe musi być multi
}

function justProps(f) {
	return f.properties;
}

function isType(t, multi = false) {
	return (f) => f.geometry.type.replace('Multi', '') === t;
}
